package game;

import game.logical_part.SeaBattleGame;

/**
 * Класс, непосредственно запускающий игру. Создаёт игру с параметрами, представленными в полях.
 * @author Малякин Кирилл. Гр 15ИТ20.
 */
public class GameLauncher {

    private static final int FIELD_SIZE = 10;
    private static final int SHIPS_COUNT = 3;
    private static final int SHIP_LENGTH = 3;

    public static void main(String[] args) {
        new SeaBattleGame().runGame(FIELD_SIZE, FIELD_SIZE, SHIPS_COUNT, SHIP_LENGTH);
    }
}
