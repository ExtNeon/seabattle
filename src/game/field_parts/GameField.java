package game.field_parts;

import java.util.ArrayList;

/**
 * Класс, представляющий игровое поле. Содержит список кораблей, автоматически создаёт их в соответствии с необходимым количеством.
 * Даёт созданным кораблям случайные имена. Проверяет выстрел и сообщает его результат в понятной текстовой форме.
 * Может целиком вывести поле в виде строки.
 * @author Малякин Кирилл. гр. 15ИТ20.
 */
public class GameField {
    private ArrayList<Ship> ships;
    private int width, height;

    private static final String MSG_WOUNDED = "Вы попали и ранили корабль ";
    private static final String MSG_DROWNED = "Вы попали и полностью потопили корабль ";
    private static final String MSG_MISSED = "Вы промахнулись";
    private static final String[] SHIP_NAMES = {"Black Pearl", "Elizabeth II", "Admiral Nahimov", "Santa Maria", "Atlantic", "Titanic"};

    ArrayList<Ship> getShips() {
        return ships;
    }

    /**
     * Создаёт поле с размерами @code{width * height}, размещает в нём @code{shipsCount} кораблей с размером @code{shipsSize}.
     * @param width ширина поля
     * @param height высота поля
     * @param shipsCount количество кораблей
     * @param shipsSize размер каждого корабля
     */
    public GameField(int width, int height, int shipsCount, int shipsSize) {
        this.width = width;
        this.height = height;
        ships = new ArrayList<>(shipsCount);

        //Creating the ships
        while (ships.size() < shipsCount) {
            ships.add(new Ship(shipsSize, this, getRandomShipName()));
        }
    }

    /**
     * Возвращает рандомное имя для корабля из заранее заданного списка. При этом, исключается повторение имён.
     * @return имя, выбранное случайным образом из заранее заданного списка.
     */
    private String getRandomShipName() {
        String result;
        boolean unique;
        int i = 0;
        do {
            result = SHIP_NAMES[(int)randomRangeGen(SHIP_NAMES.length - 1)];
            unique = false;
            for (Ship otherShip: ships) {
                unique = unique | !otherShip.getShipName().equals(result);
            }
        } while (!unique & i++ < 3000);
        return result;
    }

    /**
     * Метод генерации псевдослучайного числа из заданного промежутка
     * @param max Верхняя граница для генератора
     * @return Псевдослучайное число, от <code>min</code> до <code>max</code>
     */
    private static double randomRangeGen(double max) {
        return (Math.random()*(max-1));
    }

    /**
     * @return количество оставшихся частей кораблей
     */
    public int getLeftShipsPartsCount() {
        if (ships.size() == 0) {
            return 0;
        }
        int aliveParts = 0;
        for (Ship selectedShip: ships) {
            aliveParts += getLeftPartsOfAShipCount(selectedShip);
        }
        return aliveParts;
    }

    private int getLeftPartsOfAShipCount(Ship ship) {
        int aliveParts = 0;
        for (ShipPart selectedShipPart: ship.getParts()) {
            if (!selectedShipPart.isBlowedUp()) {
                aliveParts++;
            }
        }
        return aliveParts;
    }

    /**
     * Производит выстрел по координатам @code{x, y}, возвращает текстовое сообщение о результатах.
     * @param x положение на оси абсцисс
     * @param y положение на оси ординат
     * @return текстовое сообщение, оповещающее о результате выстрела по данным координатам
     */
    public String tryToShot(int x, int y) {
        int result;
        for (Ship selectedShip: ships) {
            if ((result = selectedShip.checkShot(x,y)) != 0) {
                return (result == 1 ? MSG_WOUNDED : MSG_DROWNED) + '\"' + selectedShip.getShipName() + '\"';
            }
        }
        return MSG_MISSED;
    }

    int getWidth() {
        return width;
    }

    int getHeight() {
        return height;
    }


    private char getShipChar(int coordX, int coordY) {
        for (Ship selectedShip: ships) {
            for (ShipPart partOfShip: selectedShip.getParts()) {
                //Magic
                if ((partOfShip.getX() == coordX) & (partOfShip.getY() == coordY)) {
                    return partOfShip.isBlowedUp()? '*' : selectedShip.getShipName().charAt(0);
                }
            }
        }
        return ' ';
    }

    /**
     * Возвращает заполненную пробелами строку, длина которой равна параметру {@code length}.
     * @param length Длина генерируемой строки
     * @return Строка длиной {@code length}, содержимое которой представляет собой повторяющиеся символы пробела
     */
    private static String genFilledSpacesString(int length) {
        StringBuilder result = new StringBuilder(); // Для экономии ресурсов
        for (int i = 0; i < length; i++) {
            result.append(" ");
        }
        return result.toString();
    }

    /**
     * @return Двумерное поле, выполненое в стиле ASCII.
     */
    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("    ");
        for (int rowIndex = -1; rowIndex < height; rowIndex++) {
            if (rowIndex == -1) {
                for (int i = 0; i < width; i++) {
                    result.append(i).append(genFilledSpacesString(4 - ("" + i).length())); //Цепной вызов. Убирает всю конкатенацию
                }
            } else {
                for (int colIndex = -1; colIndex < width; colIndex++) {
                    if (colIndex == -1) {
                        result.append((char)('A' + rowIndex));
                    } else {
                        result.append(getShipChar(colIndex, rowIndex)); //col - x, row - y;
                    }
                    result.append(" | ");
                }
            }
            result.append('\n');
        }
        return result.toString();
    }
}
