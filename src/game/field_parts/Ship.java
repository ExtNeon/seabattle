package game.field_parts;
import game.abstract_parts.Orientation;
import java.awt.*;

/**
 * Класс, представляющий корабль.
 * Может сам расположиться на поле в случайном порядке.
 * @author Малякин Кирилл, гр 15ИТ20
 */

class Ship {
    private ShipPart[] parts;
    private String shipName;
    private Orientation orientation;
    private int alivePartsCount;

    /**
     * Конструктор. Создаёт и размещает корабль размера @code{size} в переданном поле @code{field}.
     * Если размещение корабля данного размера в этом поле невозможно, выбрасывает исключение @code{RuntimeEception}
     * @param shipSize Размер корабля
     * @param field Поле, в котором требуется разместить корабль
     * @param name Имя корабля
     */
    Ship(int shipSize, game.field_parts.GameField field, String name) {
        parts = new ShipPart[shipSize];
        this.shipName = name;
        Point shipStartPoint = new Point();
        alivePartsCount = shipSize;
        int iterations = 0;
        do {
            int xMax = field.getWidth() - 1;
            int yMax = field.getHeight() - 1;
            if (randomRangeGen(-10, 10) > 0) {
                orientation = Orientation.HORIZONTAL;
                xMax -= shipSize - 1;
            } else {
                orientation = Orientation.VERTICAL;
                yMax -= shipSize - 1;
            }
            shipStartPoint.setLocation((int)randomRangeGen(0, xMax), (int)randomRangeGen(0, yMax));
            iterations++;
        } while (!canShipStandHere(shipStartPoint, field) & (iterations < 10000));
        if (iterations == 10000) {
            throw new RuntimeException("We did tried to place a ship, but we didn't do this through 10000 iterations. \n" +
                    "Tell this to developer. ErrorCode: 52");
        }
        placeShip(shipStartPoint);
        System.out.println(shipName + " was created sucessful");
    }

    /**
     * Метод генерации псевдослучайного числа из заданного промежутка
     * @param min Нижняя граница для генератора
     * @param max Верхняя граница для генератора
     * @return Псевдослучайное число, от <code>min</code> до <code>max</code>
     */
    private static double randomRangeGen(double min, double max) {
        return min + (Math.random()*(max-min+1));
    }

    /**
     * @return Все части корабля. (1 часть = 1 ячейке)
     */
    ShipPart[] getParts() {
        return parts;
    }

    /**
     * @return Имя корабля
     */
    String getShipName() {
        return shipName;
    }

    /**
     * Проверяет, можно ли поставить корабль в точке @code{firstPartCoord} с установленной ориентацией на поле @code{field}.
     * @param firstPartCoord Координата, в которой распологается первая часть корабля
     * @param field Поле, на котором требуется разместить корабль
     * @return @code{true}, если корабль может быть размещён в данной точке, @code{false} в обратном случае.
     */
    private boolean canShipStandHere(Point firstPartCoord, GameField field) {
        //Лучше всего сначала составить карту. Потом уже проверять. Так быстрее.
        //Но сначала проверим, не вылазит ли корабль за пределы поля
        switch (orientation) {
            case HORIZONTAL:
                if (firstPartCoord.getX() + parts.length >= field.getWidth()) {
                    return false;
                }
            case VERTICAL:
                if (firstPartCoord.getY() + parts.length >= field.getHeight()) {
                    return false;
                }
        }
        //Строим карту, если у нас всё в порядке.
        boolean[][] map = new boolean[field.getWidth()][field.getHeight()];
        for (int ix = 0; ix < map.length; ix++) {
            for (int iy = 0; iy < map[ix].length; iy++) {
                map[ix][iy] = false;
            }
        }
        for (Ship selectedShip: field.getShips()) {
            for (ShipPart selectedShipPart: selectedShip.getParts()) {
                map[(int)selectedShipPart.getX()][(int)selectedShipPart.getY()] = true;
            }
        }
        //Окей, теперь у нас есть карта. Проверяем, есть ли в стартовой точке карты корабль.
        if (map[(int)firstPartCoord.getX()][(int)firstPartCoord.getY()]) {
            return false;
        }

        //Теперь просканируем те места, где должны быть части корабля.
        int stX = 0;
        int sumX, sumY;
        int endX = 0;
        int endY = 0;
        int stY = 0;
        switch (orientation) {
            case HORIZONTAL:
                stX = firstPartCoord.getX() == 0.0 ? 0 : (int)firstPartCoord.getX() - 1;
                sumX = (int)firstPartCoord.getX() + parts.length - 1; //← промежуточное значение
                endX = sumX == field.getWidth() - 1 ? sumX : sumX+1;
                stY = firstPartCoord.getY() == 0 ? 0 : (int)firstPartCoord.getY() - 1;
                endY = firstPartCoord.getY() == field.getHeight() - 1 ? field.getHeight() : (int)firstPartCoord.getY()+1;
                break;
            case VERTICAL:
                stY = firstPartCoord.getY() == 0 ? 0 : (int)firstPartCoord.getY() - 1;
                sumY = (int)firstPartCoord.getY() + parts.length - 1; //← промежуточное значение
                endY = sumY == field.getHeight() - 1 ? sumY : sumY+1;
                stX = firstPartCoord.getX() == 0 ? 0 : (int)firstPartCoord.getX() - 1;
                endX = firstPartCoord.getX() == field.getWidth() - 1 ? field.getWidth() : (int)firstPartCoord.getX()+1;
                break;
        }
        for (int i = stX; i < endX; i++) {
            for (int j = stY; j < endY; j++) {
                if(map[i][j]) {
                    return false;
                }
            }

        }
        return true;
    }

    /**
     * Осуществляет выстрел по переданным координатам, и возвращает результат выстрела.
     * @param x Координата по оси абсцисс
     * @param y Координата по инвертированной оси ординат
     * @return Флаг. 0 - промах, 1 - данный корабль ранен, 2 - данный корабль полностью потоплен.
     */
    int checkShot(int x, int y) {
        for (ShipPart part: parts) {
            if (!part.isBlowedUp() && part.getX() == x && part.getY() == y) {
                part.blowUp();
                return --alivePartsCount == 0 ? 2 : 1;
            }
        }
        return 0;
    }

    /**
     * Создаёт все части корабля и размещает их, согласно ориентации корабля и точке первой части.
     * @param firstPartCoord Точка, в которой должна быть первая часть корабля
     */
    private void placeShip(Point firstPartCoord) {
        if (parts.length == 1) {
            parts[0] = new ShipPart((int)firstPartCoord.getX(), (int)firstPartCoord.getY());
            return;
        }
        int i = 0;
        for (int j = 0; j < parts.length; j++) {
            switch (orientation) {
                case HORIZONTAL:
                    parts[j] = new ShipPart((int)firstPartCoord.getX() + i++, (int)firstPartCoord.getY());
                    break;
                case VERTICAL:
                    parts[j] = new ShipPart((int)firstPartCoord.getX(), (int)firstPartCoord.getY() + i++);
                    break;
            }
        }
    }
}
