package game.field_parts;

import java.awt.*;

/**
 * Класс, описывающий часть корабля. По сути, это унаследованный класс @code{IntPoint}, к которому добавили ещё
 * одно свойство - цела ли данная часть, либо она уничтожена.
 * @author Малякин Кирилл. Гр. 15ИТ20.
 */
class ShipPart extends Point {
    private boolean blowedUp = false;

    ShipPart(int x, int y) {
        super(x, y);
    }

    boolean isBlowedUp() {
        return blowedUp;
    }

    void blowUp() {
        this.blowedUp = true;
    }
}
