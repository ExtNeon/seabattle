package game.logical_part;

import game.field_parts.GameField;

import java.util.Scanner;

/**
 * Класс, реализующий логику игры.
 */

public class SeaBattleGame {

    private static Scanner scanner = new Scanner(System.in);

    public void runGame(int fieldWidth, int fieldHeight, int shipsCount, int shipsSize) {
        GameField seaField = new GameField(fieldWidth, fieldHeight, shipsCount, shipsSize);

        String spcMessage = "";
        int iterations = 0;
        do {
            String readedStr;
            boolean isInputStrCorrect;
            do {
                System.out.println(seaField);
                System.out.println(spcMessage);
                readedStr = readLine("Введите координаты, куда вы хотите выстрелить (пример: A7): ");
                if (readedStr.charAt(0) == 'R' && readedStr.length() == 1) {
                    runGame(fieldWidth, fieldHeight, shipsCount, shipsSize);
                    return;
                }
                isInputStrCorrect = isCorrectForCoordinates(readedStr);
                if (!isInputStrCorrect) {
                    spcMessage = "Некорректно введены координаты. Попробуйте ещё раз";
                }
            } while (!isInputStrCorrect);
            spcMessage = seaField.tryToShot( Integer.valueOf("" + readedStr.charAt(1)), readedStr.charAt(0) - 'A');
            iterations++;
        } while (seaField.getLeftShipsPartsCount() > 0);
        System.out.print(spcMessage + "\nВы успешно потопили все корабли за " + iterations + " попыток.\nИгра окончена");
    }



    private boolean isCorrectForCoordinates(String readedStr) {
       return (readedStr.length() == 2) && ("ABCDEFGHIJ".contains("" + readedStr.charAt(0))) && ("0123456789".contains("" + readedStr.charAt(1)));
    }

    /**
     * Метод для запроса у пользователя какого - либо строкового значения, с выводом поясняющего сообщения.
     * @param message сообщение, выводимое пользователю
     * @return строка, введённая пользователем
     */
    private static String readLine(String message) {
        System.out.print(message);
        return scanner.next();
    }
}
