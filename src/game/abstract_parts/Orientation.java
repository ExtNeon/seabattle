package game.abstract_parts;

public enum Orientation {
    VERTICAL, HORIZONTAL;
}
